angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('SearchCtrl', function($scope, Search) {
  var topMovies;
  Search.getTopMovies().then(function(res){
    $scope.movies = res.data.results;
    topMovies = res.data.results;
  });
  $scope.resetTopMovies = function(search) {
    $scope.movies = topMovies;
  };
  $scope.searchFor = function(search) {
    Search.searchMovies(search).then(function(res){
      $scope.movies = res.data.results;
    });
  };
})

.controller('MovieCtrl', function($scope, $stateParams, Movie, $state) {
  /*Movie.getMovieTrailer($stateParams.movieId).then(function(res){
    $scope.trailer = "https://www.youtube.com/watch?v="+ res.data.results[0].key;
  });*/
  Movie.getMovieCredits($stateParams.movieId).then(function(res){
    $scope.credits = res.data.cast;
  });
  Movie.getMovieById($stateParams.movieId).then(function(res){
    $scope.movie = res.data;
    $scope.movie["credits"] = $scope.credits;
    if(localStorage.getItem("localMovies") !== null) {
      localMovies = JSON.parse(localStorage['localMovies']);
    } else {
      localMovies = new Object();
    }
    if(typeof(Storage) !== undefined) {
      $scope.addMovie = !(localMovies.hasOwnProperty(res.data.id.toString()));
    } else {
        alert("update browser");
    }
  });
    $scope.addToFavorites = function() {
        Movie.addMovieToLocalStorage($scope.movie);
        $scope.addMovie = false;
    };
    $scope.removeFromFavorites = function() {
        Movie.removeMovieFromLocalStorage($scope.movie);
        $scope.addMovie = true;
    };
})

.controller('WatchedCtrl', function($scope, Watched) {
  $scope.watched = Watched.all();
  $scope.remove = function(movie) {
    $scope.watched.splice( $scope.watched.indexOf(movie), 1 );
    Watched.removeMovieFromLocalStorage(movie.id);
  }
  $scope.doRefresh = function(movie) {
    $scope.watched = Watched.all();
  }
})

.controller('WatchedDetailCtrl', function($scope, $stateParams, Watched) {
  $scope.disable = false;
  $scope.removeMovie = function(id) {
      Watched.removeMovieFromLocalStorage($stateParams.movieId);
      $scope.watched.splice( $scope.watched.indexOf(id), 1 );
      $scope.disable = true;
    };
  $scope.watched = Watched.watchedMovie($stateParams.movieId);
});