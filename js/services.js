angular.module('starter.services', [])

.factory('Movie', function($http) {
  return {
    getMovieById: function(movieId) {
      return $http.get("http://api.themoviedb.org/3/movie/"+movieId+"?api_key=e13cd1256f394d7bcab34fe30906aa85");
    },
    addMovieToLocalStorage: function(movie) {
        var localMovies = localStorage['localMovies'];
        if(localMovies === undefined) {
          localMovies = '{}';
        }
        var moviesObject = JSON.parse(localMovies);
        movie["addDate"] = new Date();//.toISOString().substr(0, 10);
        moviesObject[movie.id] = movie; // voib-olla liiga palju informatsiooni
        localStorage['localMovies'] = JSON.stringify(moviesObject);
    },
    removeMovieFromLocalStorage: function(movie) {
      if(typeof(Storage) !== undefined) {
        var localMovies = localStorage['localMovies'];
        if(localMovies === undefined) {
          localMovies = '{}';
        }
        var moviesObject = JSON.parse(localMovies);
        delete moviesObject[movie.id];
        localStorage['localMovies'] = JSON.stringify(moviesObject);
      }
    },
    getMovieTrailer: function(movieId) {
      return $http.get("http://api.themoviedb.org/3/movie/"+movieId+"/videos?api_key=e13cd1256f394d7bcab34fe30906aa85");
    },
    getMovieCredits: function(movieId) {
      return $http.get("http://api.themoviedb.org/3/movie/"+movieId+"/credits?api_key=e13cd1256f394d7bcab34fe30906aa85");
    }
  }
})

.factory('Search', function($http) {  
  return {
    getTopMovies: function() {
      return $http.get("http://api.themoviedb.org/3/movie/top_rated?api_key=e13cd1256f394d7bcab34fe30906aa85");
    },
    searchMovies: function(search) {
      return $http.get("http://api.themoviedb.org/3/search/movie?query="+search+"&api_key=e13cd1256f394d7bcab34fe30906aa85");
    }
  }
})

/**
 * A simple example service that returns some data.
 */
.factory('Watched', function() {
  return {
    removeMovieFromLocalStorage: function(movieId) {
        var localMovies = localStorage['localMovies'];
        var moviesObject = JSON.parse(localMovies);
        console.log(movieId);
        delete moviesObject[movieId];
        localStorage['localMovies'] = JSON.stringify(moviesObject);
        return new Object();
    },
    all: function() {
      var localMovies = localStorage['localMovies'];
        if(localMovies === undefined) {
          localMovies = '{}';
        }
      jsonObject = JSON.parse(localMovies);
      movieArray = [];
      for(movie in jsonObject) {
        movieArray.push(jsonObject[movie]);
      }
      console.log(movieArray);
      return movieArray;
    }, 
    watchedMovie: function(movieId) {
      jsonObject = JSON.parse(localStorage['localMovies']);
      return jsonObject[movieId];
    }, 
  }
});
